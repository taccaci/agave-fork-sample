#!/bin/bash
set -x
INPUT="${inputFile}"
NUMLINES=${numberOfLines}

# Build up the arguments string
ARGS=""

if [ ${NUMLINES} -ne 0 ]; then
	echo "numberOfLines configured"
	ARGS="${ARGS} -n ${NUMLINES}"
fi

head $ARGS $INPUT > head_output.txt
