$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

(function() {
    $("#upload_field").html5_upload({
        url: function(number) {
            return prompt(number + " url", "/");
        },
        sendBoundary: window.FormData || $.browser.mozilla,
        onStart: function(event, total) {
            return true;
            return confirm("You are trying to upload " + total + " files. Are you sure?");
        },
        onProgress: function(event, progress, name, number, total) {
            console.log(progress, number);
        },
        setName: function(text) {
            $("#progress_report_name").text(text);
        },
        setStatus: function(text) {
            $("#progress_report_status").text(text);
        },
        setProgress: function(val) {
            $("#progress_report_bar").css('width', Math.ceil(val * 100) + "%");
        },
        onFinishOne: function(event, response, name, number, total) {
        //alert(response);
        },
        onError: function(event, name, error) {
            alert('error while uploading file ' + name);
        }
    });
});


function shortFormatDate(tdate) {
    var system_date = moment(tdate.replace(".000",""));
    var user_date = moment();

    if (moment(0, "HH").diff(system_date) <= 0) {
      return system_date.format("h:mmA");
    } else {
      return system_date.format("MMM DD, YYYY");
    }
}

function humanFileSize(bytes, si) {
    var thresh = si ? 1000 : 1024;
    if(bytes < thresh) return bytes + ' B';
    var units = si ? ['kB','MB','GB','TB','PB','EB','ZB','YB'] : ['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB'];
    var u = -1;
    do {
        bytes /= thresh;
        ++u;
    } while(bytes >= thresh);
    return bytes.toFixed(1)+' '+units[u];
};
